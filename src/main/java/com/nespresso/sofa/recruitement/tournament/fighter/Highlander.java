package com.nespresso.sofa.recruitement.tournament.fighter;

public class Highlander extends GameWar {
	private String type;

	public Highlander(String type) {
		this.type = type;
	}

	public Highlander() {
		hitPoint=150;
	}

	@Override
	public void engage(GameWar viking) {
	}

	@Override
	public int hitPoints() {
		return hitPoint;
	}

	@Override
	public GameWar equip(String equipment) {
		return null;
	}

	@Override
	public void setHitPoint(int hitPoint) {
		this.hitPoint=hitPoint;

	}

}
