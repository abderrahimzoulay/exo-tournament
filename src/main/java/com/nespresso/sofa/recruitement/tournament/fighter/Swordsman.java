package com.nespresso.sofa.recruitement.tournament.fighter;

import java.util.ArrayList;

public class Swordsman extends GameWar {
	private String type;
	private ArrayList<Equipment> equipments;

	public Swordsman() {
		hitPoint = 100;
		equipments = new ArrayList<Equipment>();
	}

	public Swordsman(String type) {
		this.type = type;
	}

	@Override
	public void engage(GameWar fighter) {
		int compteur = 0;
		while (this.hitPoint > 0) {
			int compteurOf3 = compteur + 1;
			if (fighter instanceof Viking) {
				fighterIsVicking(fighter, compteur);
			} else if (fighter instanceof Highlander) {
				fighterIsHighlander(fighter, compteur, compteurOf3);
			}
			compteur++;
		}

	}

	private void fighterIsHighlander(GameWar fighter, int compteur,
			int compteurOf3) {
		for (int i = 0; i < equipments.size(); i++) {
			if (equipments.get(i).getName().equals("armor")) {
				swordManHaveArmor(fighter, compteur, compteurOf3);
			} else {
				swordManWithHighlanderWithoutArmor(compteur, compteurOf3);
			}
			int highlanderHitPoint = fighter.hitPoints();
			fighter.setHitPoint(highlanderHitPoint -= 5);
		}
	}

	private void swordManWithHighlanderWithoutArmor(int compteur,
			int compteurOf3) {
		if (compteur % 2 == 0 && compteurOf3 % 3 != 0) {
			if (hitPoint - 12 < 0) {
				hitPoint = 0;
			} else {
				hitPoint -= 12;
			}
		}
	}

	private void swordManHaveArmor(GameWar fighter, int compteur,
			int compteurOf3) {
		if (compteur % 2 == 0 && compteurOf3 % 3 != 3) {
			hitPoint -= 9;
		}
		int highlanderHitPoint = fighter.hitPoints();
		fighter.setHitPoint(highlanderHitPoint -= 4);
	}

	private void fighterIsVicking(GameWar fighter, int compteur) {
		if (equipments.size() != 0) {
			for (int i = 0; i < equipments.size(); i++) {
				if (equipments.get(i).getName().equals("buckler")) {
					swordsManHaveABucklerButDestroyedAfter3Blow(compteur);
					vickingHaveAbuckler(fighter, compteur);
				} else {
					reduceNumberOfPointOfSwordsManWithoutEquipmentsVSVicking();
					reducePointsOfVickingWithoutEquipments(fighter);
				}
			}
		} else {
			reduceNumberOfPointOfSwordsManWithoutEquipmentsVSVicking();
			reducePointsOfVickingWithoutEquipments(fighter);
		}
	}

	private void reducePointsOfVickingWithoutEquipments(GameWar fighter) {
		int fighterHitPoint = fighter.hitPoints();
		if (fighterHitPoint - 5 < 0) {
			fighter.setHitPoint(0);
		} else {
			fighter.setHitPoint(fighterHitPoint -= 5);
		}

	}

	private void reduceNumberOfPointOfSwordsManWithoutEquipmentsVSVicking() {
		if (hitPoint - 6 < 0) {
			hitPoint = 0;
		} else {
			hitPoint -= 6;
		}

	}

	private void swordsManHaveABucklerButDestroyedAfter3Blow(int compteur) {
		if (compteur <= 4) {// blow when compteur is in 0 2 4
			if (compteur % 2 == 0) {
				reduceNumberOfPointOfSwordsManWithoutEquipmentsVSVicking();
			}
		} else {
			reduceNumberOfPointOfSwordsManWithoutEquipmentsVSVicking();
		}
	}

	private void vickingHaveAbuckler(GameWar fighter, int compteur) {
		if (compteur % 2 == 0) {
			reducePointsOfVickingWithoutEquipments(fighter);
		}
	}

	@Override
	public int hitPoints() {
		return hitPoint;
	}

	@Override
	public GameWar equip(String equipment) {
		Equipment equip = new Equipment();
		equip.setName(equipment);
		equipments.add(equip);
		return this;
	}

	@Override
	public void setHitPoint(int hitPoint) {
		this.hitPoint = hitPoint;
	}

}
