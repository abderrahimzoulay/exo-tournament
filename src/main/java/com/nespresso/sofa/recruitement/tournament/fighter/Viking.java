package com.nespresso.sofa.recruitement.tournament.fighter;

public class Viking extends GameWar {

	public Viking() {
		hitPoint = 120;
	}

	@Override
	public void engage(GameWar viking) {
	}

	@Override
	public int hitPoints() {
		return hitPoint;
	}

	@Override
	public GameWar equip(String equipment) {
		return null;
	}

	@Override
	public void setHitPoint(int hitPoint) {
		this.hitPoint=hitPoint;

	}

}
