package com.nespresso.sofa.recruitement.tournament.fighter;

public abstract class GameWar {
	protected int hitPoint ;
	public abstract void engage(GameWar viking);
	public abstract int hitPoints();
	public abstract void setHitPoint(int hitPoint);
	public abstract GameWar equip(String equipment);
	
	
	
}
